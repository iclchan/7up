package com.iclchan;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class UpApplicationTest {

    @Test
    public void canValidate() {
        UpApplication upApplication = new UpApplication();

        assertTrue(upApplication.check(63));
        assertFalse(upApplication.check(64));
        assertFalse(upApplication.check(65));
        assertFalse(upApplication.check(66));
        assertTrue(upApplication.check(67));
        assertFalse(upApplication.check(68));
        assertFalse(upApplication.check(69));
        assertTrue(upApplication.check(70));
    }
}
