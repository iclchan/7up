package com.iclchan;

public class UpApplication {

    public void run() {
        int start = 0;
        int end = 100;

        for (int i = start; i < end; i++) {
            boolean shouldShout = check(i);

            if (shouldShout) {
                System.out.println("Up!");
            } else {
                System.out.println(i);
            }
        }
    }

    public boolean check(int target) {
        return contains7(target) || isMultipleOf7(target);
    }

    private boolean contains7(int target) {
        String targetString = Integer.toString(target);
        return targetString.contains("7");
    }

    private boolean isMultipleOf7(int target) {
        return (target % 7 == 0 && target != 0);
    }
}
